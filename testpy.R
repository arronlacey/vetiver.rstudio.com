reticulate::virtualenv_install(
  envname = "/root/venv",
  packages = c("matplotlib")
)

reticulate::use_virtualenv("/root/venv")

reticulate::py_run_string("import matplotlib.pyplot as plt;plt.plot([1, 2, 3], [1, 2, 3])")
